// modules
var gulp = require('gulp'),
	connect = require('gulp-connect'),
	
	// utils
	plumber = require('gulp-plumber'),
	beep = require('./beep'),
	concat = require('gulp-concat'),
	
	// js
	uglify = require('gulp-uglify')
	;

// paths
var paths = require('./paths');

// task
var tasks = function () {
	gulp.src(paths.js.src)
		.pipe(concat(paths.js.name))
		.pipe(uglify(paths.js.name))
		.pipe(gulp.dest(paths.dest))
		.pipe(connect.reload())
		;
}

// module
module.exports = tasks;