var gulp = require('gulp'),
	flatten = require('gulp-flatten');

var paths = require('./paths');

var task = function () {
	gulp.src(paths.fonts.src)
		.pipe(flatten())
		.pipe(gulp.dest(paths.fonts.dest));
}

module.exports = task;